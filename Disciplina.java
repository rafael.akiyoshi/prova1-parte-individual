package cadastro;

import java.util.ArrayList;

public class Disciplina{

	private String nomeDisciplina;
	private String codigo;
	private ArrayList<Aluno> alunosMatriculados = new ArrayList<Aluno>();

	public Disciplina(){
		
	}
	
	public Disciplina(String umNomeDisciplina , String umCodigo){
		nomeDisciplina = umNomeDisciplina;
		codigo = umCodigo;
	}
	
	public void setAlunosMatriculados(ArrayList<Aluno> listaMatriculados){
		alunosMatriculados = listaMatriculados;
	}
	
	public ArrayList<Aluno> getAlunosMatriculados(){
		return alunosMatriculados;
	}

	public void setNomeDisciplina(String umNomeDisciplina){
		nomeDisciplina = umNomeDisciplina;
	}

	public String getNomeDisciplina(){
		return nomeDisciplina;
	}


	public void setCodigo(String umCodigo){
		codigo = umCodigo;
	}

	public String getCodigo(){
		return codigo;
	}

	


	
}
