package cadastro;

import java.util.ArrayList;
public class ControleDisciplina{

	private ArrayList<Disciplina> listaDisciplinas;

	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();
	}
	
	public void adicionarAlunoEmDisciplina(Aluno umAluno, Disciplina umaDisciplina){
		umaDisciplina.getAlunosMatriculados().add(umAluno);
	}

	public String adicionar(Disciplina umaDisciplina){
		String mensagem = "Disciplina Adicionada com Sucesso!";
		listaDisciplinas.add(umaDisciplina);
		return mensagem;
	}

	public String remover(Disciplina umaDisciplina){
		String mensagem = "Disciplina Removida com Sucesso!";
		listaDisciplinas.remove(umaDisciplina);
		return mensagem;
	}

	public Disciplina pesquisarNomeDisciplina(String umNomeDisciplina){
		for(Disciplina umaDisciplina: listaDisciplinas){
			if(umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNomeDisciplina)) return umaDisciplina;
			}
		return null;
	}
}
